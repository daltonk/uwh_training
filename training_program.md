Underwater Hockey Program
=========================
Alan Kucar 2019

Underwater Hockey Program consists of four parts. First part is warm-up and calibration or proper adaptation of the body to the water. The second part are puck handling techniques. The third part is focused more on different techniques of underwater movement and parts of tactic play which are used in every tactic possible. The fourth part will be focused on different tactics and their pros and cons. Due to time constraints it is impossible to do all the exercises on single training, it is better to build sets out of these and repeat them in turns.

## Warm-up & Calibration

Focus should be on finishing the exercises while trying to calibrate and adapt your body to the underwater environment. It should be a meditative time where players focus on how their bodies feel and move through the water. What we are trying here to adapt to is CO2 saturation which alarms our body receptors initiating a sort of body panic which forces us to resurface much sooner than needed. It is not the pain which has to be endured but something that our bodies in the normal air filled environment are not used to, we are adapting to different body reactions in a new environment. And to be clear, underwater hockey has nothing to do with STATIC APNOEA! No one is allowed to do static breath holding as an add-in exercise while on training and without special supervision! Static apnoea is dangerous.

	+ 1 lane underwater without fins - relaxed, slow, focused on your body reactions while trying to reach the end
	+ 2-8 lanes only legs sideways - no fins (younger players 2-4 while national team players 4-8)
	+ 4 x 4 Bouncing >  1/2 Under 1/2 Above > Whole Under > Whole Above
	+ 1 lane sprint > 10 seconds breathing > 1/2 Under 1/2 Above > 30 seconds breathing > repeat (4-8x)
	+ 1 lane slow (all players together) > 30 seconds breathing > repeat (4x)
	+ 1/2 lane Above 1/2 Under & pause
		with every new lane 5 seconds is taken from overall time allowed to finish the lane 
		start > 1 min > 55 sec > 50 sec > 45 sec > ... > until no one finishes on time 

## Puck Handling

Puck handling techniques or skills should be repeated almost indefinitely. The hardest part in puck handling is getting the mechanics right. Following videos should help you to study the skills more closely which means that the skill mechanics should be studied already before the pool training. There are skills which can be used almost in every situation with a slight modification but there are skills which are useful only in certain situations. After basic skills are figured out, to the point of doing them without looking at the puck and while swimming, the next step is to start creating combinations between them. Skills should as well be always trained in the training game.

	+ rolling puck
	+ left wall tricks
		* pull > inverse push > flick
		* inverse pull > push > flick
		* wall slide > catch > push > flick or dummy
		* offense sandwich
	+ right wall tricks
		* pull > inverse push > flick (2 options)
		* inverse pull > push > flick
		* wall slide > catch > inverse push > flick or dummy
		* offense sandwich
	+ dummy
		* small
		* medium
		* big
		* inverse
	+ flicks
		* flick and follow
		* flick left & right
		* low & high flicks
		* inverse flick
		* cowboy flicks
		* inside flick - normally in front of the goal
		* inverse inside flick
	+ defence skills
		* keeping distance
		* roll off
		* sideway pickup
		* wrong way sucker!
		* roll barell
	
## Underwater Movement Techniques

	// switch
		> fronts
		> backs
	// backs rotation & fronts switch in front 
	// backs switch & fronts interception(block)
	// strike
		> direct strike
		> side strike
	// bin attack
		> from the corner (wall & switch)
		> from the side (switch)
		> from frontal (advantage puck)
	// bin defense
		> when attacked frontally (advantage puck or switch) 
		> corner defense

#### Rotations - returning to the correct position in the team structure
	
Whenever a player finishes on the bottom they have to return to the surface position so they can engage in the game frontally. This means they have to eject themselves into direction from which the puck the puck will always be in front of them, never behind them or underneath them (in some situations the former can be useful). This allows the players to predict better game positioning, control the speed and timing of their movements toward the puck.

	+ Forwards
	+ Backs

#### Diving

	+ seal diving
	+ duck diving
	+ criss-cross - 3 players move with puck away from the center and flick it back to the center to the middle
	+ rotations 

### Switch to sandwich play

![switch vs. penetration zones](img/web_01_switch.svg "switch vs. penetration zones")

pic ~ 1, _switch vs. penetration zones_

When doing the switch the first goal is to escape in to the open space, space where there is no opposite player, space where players can look around and organize next step in the overall strategy. Biggest mistake new players always make is they are trying to go through the opponents structure either directly towards the bin or on the wall instead of just going around it (pic ~ 1). It is the shortest way to get there but it is as well the hardest way. Underwater hockey is a space game and shorter and faster pathways to the bin are normally protected. So what we are left with is either fighting through and use much of the teams energy or go around these protected areas and try finding better ways to reach the bin. The best way to do this is by switching the teams structure from one side of the pool to the other, up an down & diagonally. 

![backline switch](img/web_02_switch.svg "backline switch")

pic ~ 2, _backline switch_

Switch can be executed either as the backline switch or forward line switch. The differences are on the level of vulnerability, difficulty of execution and amount of open space available. While executing switch on the backline switch (pic ~ 2) can be easier, with much more space available, it can be as well very dangerous, the only thing preventing the opposition attack our bin is by cutting through the backline. This is why backs have to always position themselves diagonally. On the other hand the forward line switch (pic ~ 3) is in comparison much harder to execute, with very little space available, but very dangerous for the opposition, same way as for our backs when doing the switch. What the switch gives us is the chance to play against fewer opponents on a part of the pool terrain, creating the openings in the opponents structure allowing us to find better ways to reach the bin. The less players are in front of us the bigger are the opportunities to score.

![forward line switch](img/web_03_switch.svg "forward line switch")

pic ~ 3, _forward line switch_

Following game exercises are constructed in a way of simulating the proper switch situation, where players who initiate the switch are being covered by opposing players. Opposing players have to think about how to react when being switched from one to another side of the pool. This is very important because both side have to focus on their positioning. Attack and defence are both equally important.

####	2 on 2 switch exercises

The forwards are switching to the other side and bringing the puck as close as possible to the bin while backs are following and trying to escort the forwards away from the bin, locking them on the nearest wall. Because the backs are normally able to block the attack and lock the forward on the next wall the available forward has a duty to help out the locked forward by sandwiching the defending back. After receiving the puck the forward continues to progress towards the bin where already the next back is trying to block and defend the bin. This type of long switch gives a team much more space and time to organize, it is more logical when one knows where the next end point of next positioning. In other words the forward doing the next sandwich does not have to think much where he have to be next - forward has to sandwich on the wall. One other advantage here is that the forward following the one with puck can breathe on the surface longer. But even though the forward on the surface can breathe he still has to swim much faster on the surface than the forward pushing the puck on the bottom, he has to catch him at the moment of passing the puck over the stick. The opponent, their backs have to be really careful not to engage with the forwards if they are not sure their they have a backup to cover their mistakes. The most dangerous thing for a backline is to find themselves running after some forward who dummied their back somewhere in the middle of the pool. Proper distancing makes sure there are no unexpected flicks which could open up the fast way to the bin. When handling the opponents switches backs should regard themselves as a sort of an escort service which escorts the forwards from one wall to the other, locks them there and disables their chances to move closer to the bin. They should leave the heavy lifting to the forward line. This becomes much more clear when we move from a classic 3-3 to a 1-3-2 formation.

![long 2 on 2 switch](img/web_04_switch.svg "long 2 on 2 switch")

pic ~ 3, _long 2 on 2 switch_

If the first option appears to be more passive and there is less need to figure out where the next positioning is located then the second option becomes much more aggressive. Here the forwards try to attack the bin from the mid pool. After the switch was initiated the forward who flicked the puck has to eject himself out of the water in such a way that his trajectory is aligned with the direction of the switch. This will spare some energy and time needed for next encounter or move to succeed. One other important clue of where the next forward can expect receiving the puck is the trajectory between the player with the puck and the center of the bin. Because there are so many possibilities of where the next exchange point of the puck between the two forwards will happen this requires much more precise timing and positioning than the previous version (pic ~ 3). We don't want to be either too late nor to soon there for the opponents backs to cut our intentions and create a counter attack situation. As for the opponents backs, they have to be careful not to tackle the forwards if there is no support behind which will cover their mistakes.

![variable 2 on 2 switch](img/web_05_switch.svg "variable 2 on 2 switch")

pic ~ 5, _variable 2 on 2 switch_

####	3 on 3 exercises

Three forwards against three backs. Same as in the 2 on 2 exercise, forwards here try to execute the switch in order to escape the wall mess into the open space where the game becomes much more predictable and organized. One key difference between the switch with only two players and this one is that with three backs behind (3-3 formation) the backline is allowed to take risk with cutting the switches. Here there are enough players in the backline who can intercept unwanted escapes. 

![long 3 on 3 switch](img/web_06_switch.svg "long 3 on 3 switch")

pic ~ 6, _long 3 on 3 switch_

![variable 3 on 3 switch](img/web_07_switch.svg "variable 3 on 3 switch")

_variable 3 on 3 switch_

#### 4 on 4


## Tactical Play

There are many ways how a team could position itself to gain advantage over the opposition but before we jump onto positioning itself we have to understand what it means to be in possession of the puck. One of the most visible and common mistakes that team makes are during the advantage puck, I have never seen a part of the game so unused as an execution of advantage puck. 

It sometimes appears as if no one understands that team has an "advantage" and, as with in the previous topic mentioned use of empty pool space, in most cases advantage puck is executed as if there was no space at all, team moving straight forward in a form of a brick. So when in possession of the puck the team should start thinking more confidently of using advantages that possession of the puck implies.

####	Play strategy in relation to pool sections
	
	+ On the Wall - Offensive - Switch Fronts
	+ On the Wall - Offensive - Switch Backs
	+ In the Corner - Offensive
	+ Offence in front of the Goal
	+ On the Wall - Defensive - Switch Fronts
	+ On the Wall - Defensive - Switch Backs
	+ In the Corner - Defensive
	+ Defence in front of the Goal

####	Play strategy in relation to events

	+ Strike
	+ Advantage Puck - Defensive & Offensive
	+ Equal Puck
	+ Penalty Shot
	
## Formations
	+ 3 - 3 the classic 
	+ 1 - 3 - 2 this is the formation we want to learn
	+ 2 - 3 - 1 this formation will destroy

