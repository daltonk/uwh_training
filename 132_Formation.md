# 1 - 3 - 2 Formation

1-3-2 formation is very much similar to already classic 3-3 formation both in how the forward and back line interact to each other as well in how these two cover the space. The main difference between the two can be found in how one player is taken from the defence/backline and put to use as a freelance player, a sort of shadow player who is looking over the team by intervening at crucial moments, either when team structure is at its most vulnerable in defence or when excelling in offence but needs that little push to destroy the opposition's backline. What might appear to be a coincidental vulnerability, a missing player in the backline, turns out to be well planned 3-2 structure with an extra player not accounted by the opposition. In other words the opposition has to be always on alert, expecting that one missing player ambushing their play. Of course this means that the remaining part of 3-2 formation structure has to be more rigid in its functioning than 3-3 formation, where former still allows some breathing space for backline to be "creative". In a sense 3-2 part has to do the work of a 3-3 formation. Here the backline, the two remaining backs have no creative freedom whatsoever. Everything has to be by the book, even as far as how and when are they allowed to engage in switch at the same time, which is almost a no go zone. The only freedom they have is when tackling the opponent one on one, trying to out skill them to bring the puck forwards. If the backline is in a sense locked in its working then the forward line can at least be creative when we need to breakout, one on one duels and that last moments when we need to score. Every positioning and movement surrounding these moments from strike, wall, corner, bin play has to be as rigorous as with backline.

## Wall Positioning and options

### Forward on the wall 
-----
(in this situation LF)

#### Offense

1. Flick behind opponents back and follow until your S picks up the puck. Eject yourself out of the water towards the direction where the S is moving and prepare yourself for the next sandwich.

2. Penetrate through on the wall and flick behind the next opponent back in line. Follow until your S picks up the puck. Eject yourself out of the water towards the direction where the S is moving and prepare yourself for the next sandwich.

3. Wait for your C on the right to come closer and pass the puck to him so he can initiate the switch. Eject yourself out of the water towards the direction where the C is moving and prepare yourself for the next sandwich.

4. Move the puck from the wall and initiate the switch. When the opponent's back tries to tackle you flick the puck over the stick and follow until the C picks up the puck and continues with the switch. Eject yourself out of the water towards the direction where the C is moving and prepare yourself for the next sandwich.

#### Defense

1. Wait behind the opponent's forward horizontally so you are able to see both opponent's forward and back - you are sandwiching him. When the opponent's forwards turns take the puck away from him and go into the offense mode, which means you have to decide which option is the most appropriate for your situation. 

2. Wait behind the opponent's forward horizontally so you are able to see both opponent's forward and back. Your back flicks the puck over their forward which means you are now in offense mode. Now you have to decide which option is the most appropriate for your situation.

3. Wait behind the opponent's forward horizontally so you are able to see both opponent's forward and back. Your back gets overplayed and their forward gets blocked by another one of your backs. If you can still use your bottom time swim towards the forward being blocked by your back and wait for one of the above options to happen or eject yourself out of the water towards the back and rotate your body in such a way that you will have both puck and opponent's bin in front of you. This way if your back has managed to stop the opponent on the wall you are back in the Offense mode. Now you are going down hovering over your back and waiting for your back to flick the puck behind the opponent's forward. 

* Here is another option where after ejecting yourself toward the back you submerge and fill the space behind the opponent's forward, wait for one of the above options (1,2) to happen. I would recommend such an option only when the team is  trying to defend as to disable any option for the opponent to open the game. This means our forward will first of have difficulty turning his body into offensive mode without being tackled by opponent or even loosing the puck. Other thing is wasting your oxygen levels which are needed for the offense mode. Executing surface rotation gives you enough time to breathe and more oversight over the next possible move your back or backs will make. They might not even go back your way.

### Center
-----

#### Offense

1. When Forward on the wall moves or flicks behind opponent's Back you follow that puck until it reaches S player. If the puck fails to reach its destination then you are allowed to intervene and continue as S player. This means the S now has to fill the gap which was created when you left your post. S is now Center until next swap.

2. If the Forward on the wall decides to flick the puck to you then this means you are already at the bottom close enough to be passed to. You moved to him close enough to get noticed. One other way to get the puck this way - without the forward noticing - is for him to make a blind pass.  It is possible but this is for another reality.  I would argue here that hockey players always have to know where they are passing there picks to so this sort of passing we would leave for another reality. After accepting the puck there are few options which can be decided upon understanding the space around you.

  + a) If the space to your right - the opposite side of the nearest wall - is empty this means you can initiate the switch. You swim into the open space and continue until you are tackled by the opponent's Back, then you flick over his stick and follow the puck until it is picked up by your Off-Forward who will continue to execute the switch. Eject yourself out of the water towards the direction where the Off-Forward is moving and prepare yourself for the next sandwich.

  + b) Same as the above option but without the pass to Off-Forward. It can happen that your Forward is late for some reason or you notice he is already going to be blocked by their other back. Here you can pass to your Off-Back whose role is to protect the execution of the switch. It is advisable not to misuse this option because the opponent can develop an option to use this to his advantage and cut your switch to attack our bin. After the flick and when the Off-Back picked up the puck you eject yourself out of the water towards the direction where the Off-Back is moving and prepare yourself for the next sandwich. In this situation Off-Back is replacing the Off-Front who then has to replace the missing Back until they can safely change their positions.

  + c) If the space to your right is blocked you move forward and flick the puck behind the opponent's Back who tries to tackle you. After the flick you follow the puck until it is picked up by your S player. Of course if you get the puck after you flicked over the back you can continue until the next obstacle where you flick and follow once more.

  + d) You go to the wall and lock the puck on the wall. This option is normally reserved when we know that the team has to defend much more thoroughly, when the team has to take a short break or make a necessary substitution. After the center goes to the wall there has to be someone who will substitute him on his position. In this case the Forward who should have been on the wall moves to Center's position until they both can safely exchange their positions.

#### Defense

1. Wait behind the opponent's forward horizontally so you are able to see both opponent's forward and back, your body is positioned somewhere in the mid depth from where you are hidden to the opponent but at the same time close enough to intervene. When the opponent's Forward turns take the puck away from him and go into the offense mode, which means you have to decide which option is the most appropriate for your situation. 

2. When your Forward on the wall needs to go up you can replace his position and play the options available to him. You play there until you and your Forward can both safely exchange your positions. (There is a possibility that in such situation the Back will intervene and substitute the Forward. To prevent such occurrence it is better to make clear who can intervene in such cases - Back or Center?)

### Off-Forward
-----

If the game is on the wall you stay on your side and sweep the terrain and look for one of the offensive or defensive options to occur. 

#### Offense

1.  When Center (or in rare cases Back) initiate the switch you are the one who drives the puck into the open space. There you have three options: 

  + try to outrun the opponent's who are trying to block you and score

  + drive the puck to the next wall (opposite wall or the one where the bin is), flick over the opponent's stick and follow the puck until one of your players (Center, LF) will pick up the puck. Eject yourself towards the direction of the player with the puck and get ready for the next sandwich.

  + drive the puck to the middle of the pool, flick the puck over the stick of the opponent following you and follow the puck until one of your players (Center, LF) will pick up the puck. Continue to sandwich the opponent or eject yourself towards the direction of the player with the puck and get ready for the next sandwich.

2. If your Center flicks to the S you support him by sandwiching the opponent who is trying to block him and wait for the possible flick from S or wait for the opponent to turn and then take the puck away from him. In such case you are probably the  closest player to score, do it.

3. If your Center dummies the opponent and moves forward you support him with sandwiching the opponent who is trying to block him and wait for the possible flick from Center or wait for the opponent to turn and then take the puck away from him. If the path to the bin is empty go for it, if you get blocked by the opponent flick over his stick and follow until your S picks up the puck, eject yourself out of the water towards the direction where S is moving and get ready for the next sandwich.

#### Defense

 1. If the the opponent Backs try to do the switch you are the one to prevent it by either escorting (blocking the space with your body) the opponent to the next wall or take the puck when he is not looking. Second option is counter-offensive one which can open options for attacking the Bin. But at the same time failing to take the puck it can pose a threat for the opponent to move in the open space and attack our Bin. The second option is to be used when we are playing on the other half side of the pool and when we are sure we can ambush the opponent without him noticing us. 

2. If the opponent cannot be stopped by our two Backs you have to go and play as a third Back. But only when the opponent breaks through! In most cases Backs can manage it so you will go there mid depth, do the check and return to previous position. If they break through you don't take the puck from the opponent and lock it on the wall. Wait for your backs to take over and get back to your primary position.

3. If the opponent's Forward Line initiates the switch you have to help your Back to escorting the Forward to the next wall by sandwiching him all the way. If the opponent's Forward turns you take the puck away from him and move to the offensive mode.


### Back on the wall
-----
(in this situation LB)

#### Offense

1. If the you are in possession of the puck you flick over the opponent's stick and follow the puck until it is picked by our forward, eject yourself out of the water towards the direction where the game is moving and position yourself behind your other Back who is now replacing you.

2. If there is no one in front or if you manage to dummy the opponent then you continue to swim until the next obstacle where you flick over the opponent's stick and follow the puck until it is picked by our forward, eject yourself out of the water towards the direction where the game is moving and position yourself behind your other Back who is now replacing you.

3. If our Forward on the wall is moving forward you have to move behind him in such a way that in case he gets tackled by the opponent you can intervene, get the puck from the opponent and flick it back to our Forward waiting for you. It can as well happen that not Center or S are there to accept the puck as well. After the flick you follow until it is picked up by our player, eject yourself out of the water towards the direction where the game is moving and position yourself behind your other Back who is now replacing you.

#### Defense

You are technically always in the defense. Your role is to occupy the space on the wall and prevent the opponent to break through that space. If you succeed in taking the puck away then you are in offensive mode and play one of the available Offensive options. If you don't succeed and they break through you are then bound to position yourself at least in such a way that you are blocking their way and they are forced either to flick over your stick or to go around you. After that you eject yourself out of the water towards the direction where the game is moving and position yourself behind your other Back who is now replacing you.
